# Usage of the two examples

### Generation of Charuco Board
`./create_charuco -d=0 -h=7 --ml=150 --si=true --sl=240 -w=5 -m=10 board.png`

### Camera calibration
`./charuco_cal -d=0 -a=1 -h=7 -w=5 --ml=150 --sl=240 --zt=true out.xml`
